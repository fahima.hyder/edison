import boto3
import datetime
import os
import mysql.connector
import configparser
import zipfile
import gzip
import shutil

reader = configparser.ConfigParser()
reader.read('creds2.cnf')

s3_writer = boto3.client('s3', aws_access_key_id = reader.get('aws_writer', 'aws_access_key_id'), aws_secret_access_key = reader.get('aws_writer', 'aws_secret_access_key'))
s3 = boto3.client('s3', aws_access_key_id = reader.get('aws', 'aws_access_key_id'), aws_secret_access_key = reader.get('aws', 'aws_secret_access_key'))

db = mysql.connector.connect(host = reader.get('ambermire','host'), user = reader.get('ambermire','user'), password = reader.get('ambermire','password'), database = 'import_edison_data', allow_local_infile = True)
db.autocommit = True
print('connected')
cursor = db.cursor(buffered=True, dictionary=True)

##change yesterday to today
x = datetime.date.today()
#yesterday = x - datetime.timedelta(days = 1)

path_date = f"{x.month}_{x.day}_{x.year}"

##Create backup tables if it's the beginning of the month
if x.day == 1:
    print(x.day, " is the firstday of the month")
    cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.receipts_{path_date}_raw_bkup;")
    cursor.execute(f"CREATE TABLE IF NOT EXISTS import_edison_data.receipts_{path_date}_raw_bkup LIKE import_edison_data.receipts_base_format;")
    cursor.execute(f"INSERT IGNORE INTO import_edison_data.receipts_{path_date}_raw_bkup SELECT * FROM import_edison_data.receipts_raw;")
    cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.receipts_{path_date}_bkup;")
    cursor.execute(f"CREATE TABLE IF NOT EXISTS import_edison_data.receipts_{path_date}_bkup LIKE import_edison_data.receipts_base_format;")
    cursor.execute(f"INSERT IGNORE INTO import_edison_data.receipts_{path_date}_bkup SELECT * FROM import_edison_data.receipts;")
    cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.users_{path_date}_raw_bkup;")
    cursor.execute(f"CREATE TABLE IF NOT EXISTS import_edison_data.users_{path_date}_raw_bkup LIKE import_edison_data.users_base_format;")
    cursor.execute(f"INSERT IGNORE INTO import_edison_data.users_{path_date}_raw_bkup SELECT * FROM import_edison_data.users_raw;")
    cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.users_{path_date}_bkup;")
    cursor.execute(f"CREATE TABLE IF NOT EXISTS import_edison_data.users_{path_date}_bkup LIKE import_edison_data.users_base_format;")
    cursor.execute(f"INSERT IGNORE INTO import_edison_data.users_{path_date}_bkup SELECT * FROM import_edison_data.users;")

else:
    print(x.day, ' is not the first day of the month')



x = str(x)

#Signal Import

#BACKUP TABLE
cursor.execute("DROP TABLE IF EXISTS import_edison_data.signal_bkup;")
cursor.execute("CREATE TABLE IF NOT EXISTS import_edison_data.signal_bkup LIKE import_edison_data.signal_base_format;")
cursor.execute("INSERT IGNORE INTO import_edison_data.signal_bkup SELECT * FROM import_edison_data.signal")

#DOWNLOAD FILE FROM S3 ONTO SERVER AND IMPORT TO DATABASE
cursor.execute('''SET @upload_start_time = CONVERT_TZ(NOW(),'UTC', 'America/New_York');''')
cursor.execute(f'''INSERT IGNORE INTO edison_pull_log VALUES ( NULL, @upload_start_time, NULL, "signal/signal_{x}.csv", "/home/ec2-user/edison/signal/signal_{x}.csv");''')
cursor.execute('''UPDATE edison_pull_log SET file_type = "Signal" WHERE time_elapsed IS NULL;''')
print('superdata-receipts-data', f'signal/signal_{x}.csv', f'/home/ec2-user/edison/signal/signal_{x}.csv')
s3.download_file('superdata-receipts-data', f'signal/signal_{x}.csv', f'/home/ec2-user/edison/signal/signal_{x}.csv')
cursor.execute(f'''LOAD DATA LOCAL INFILE '/home/ec2-user/edison/signal/signal_{x}.csv' INTO TABLE import_edison_data.signal FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'  LINES TERMINATED BY '\n' IGNORE 1 LINES''')
cursor.execute('''SET @max_time_start = (SELECT MAX(time_start) FROM edison_pull_log);''')
cursor.execute('''UPDATE edison_pull_log SET time_elapsed = TIMEDIFF(CONVERT_TZ(NOW(),'UTC', 'America/New_York'), @max_time_start) WHERE time_elapsed IS NULL;''')

#UPLOAD TO S3 AND DELETE FROM SERVER
s3_writer.upload_file(f'/home/ec2-user/edison/signal/signal_{x}.csv', "sdr-transfer-sftp", f"edison/signal/signal_{x}.csv")
os.remove(f'/home/ec2-user/edison/signal/signal_{x}.csv')




##User Panel Import

#SERVER PATH
path = f"/home/ec2-user/edison/user_panel_2018-08-01/"

#CREATE S3 BUCKET DIRECTORY
bucket_name = "sdr-transfer-sftp"
directory_name = f"edison/user_panel/{x}" #it's name of your folders
s3_writer.put_object(Bucket=bucket_name, Key=(directory_name+'/'))

#BACKUP TABLE
cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.users_raw_bkup;")
cursor.execute(f"RENAME TABLE import_edison_data.users_raw TO import_edison_data.users_raw_bkup;")
cursor.execute(f"DROP TABLE IF EXISTS import_edison_data.users_raw;")
cursor.execute(f"CREATE TABLE IF NOT EXISTS import_edison_data.users_raw LIKE import_edison_data.users_base_format;")

#DOWNLOAD FILES TO SERVER
files = []
objects = s3.list_objects(Bucket='superdata-receipts-data', Prefix = f'user_panel_2018-08-01/{x}')
for obj in objects['Contents']:
    print(obj['Key'])
    files.append(obj['Key'])
for i in files:
    cursor.execute('''SET @upload_start_time = CONVERT_TZ(NOW(),'UTC', 'America/New_York');''')
    cursor.execute(f'''INSERT IGNORE INTO edison_pull_log VALUES (NULL, @upload_start_time, NULL, "superdata_user_{x}_00{files.index(i)}.csv", "{path}/superdata_user_{x}_00{files.index(i)}.csv");''')
    cursor.execute('''UPDATE edison_pull_log SET file_type = "User_Panel" WHERE time_elapsed IS NULL;''')
    s3.download_file('superdata-receipts-data', i, f'{path}/superdata_user_{x}_00{files.index(i)}.csv.gz')

    #UNZIP
    with gzip.open(f'{path}/superdata_user_{x}_00{files.index(i)}.csv.gz', 'rb') as f_in:
        with open(f'{path}/superdata_user_{x}_00{files.index(i)}.csv', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
            os.remove(f'{path}/superdata_user_{x}_00{files.index(i)}.csv.gz')

    #LOAD TO TABLE
    cursor.execute(f'''LOAD DATA LOCAL INFILE '{path}/superdata_user_{x}_00{files.index(i)}.csv' INTO TABLE import_edison_data.users_raw FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES;''')
    cursor.execute('''SET @max_time_start = (SELECT MAX(time_start) FROM edison_pull_log);''')
    cursor.execute('''UPDATE edison_pull_log SET time_elapsed = TIMEDIFF(CONVERT_TZ(NOW(),'UTC', 'America/New_York'), @max_time_start) WHERE time_elapsed IS NULL;''')

    #UPLOAD TO S3
    s3_writer.upload_file(f'{path}/superdata_user_{x}_00{files.index(i)}.csv', bucket_name, f'{directory_name}/superdata_user_{x}_00{files.index(i)}.csv')
    os.remove(f'{path}/superdata_user_{x}_00{files.index(i)}.csv')


##Receipts

##SERVER PATH
path = "/home/ec2-user/edison/receipts_2018-08-01/"

#CREATE S3 BUCKET DIRECTORY
bucket_name = "sdr-transfer-sftp"
directory_name = f"edison/receipts/{x}" #it's name of your folders
s3_writer.put_object(Bucket=bucket_name, Key=(directory_name+'/'))

#DOWNLOAD FILE ONTO SERVER
files = []
objects = s3.list_objects(Bucket='superdata-receipts-data', Prefix = f'receipts_2018-08-01/{x}')
for obj in objects['Contents']:
   print(obj['Key'])
   files.append(obj['Key'])
for i in files:
    cursor.execute('''SET @upload_start_time = CONVERT_TZ(NOW(),'UTC', 'America/New_York');''')
    cursor.execute(f'''INSERT IGNORE INTO edison_pull_log VALUES (NULL, @upload_start_time, NULL, "{x}_00{files.index(i)}.csv", "{path}/{x}_00{files.index(i)}.csv");''')
    cursor.execute('''UPDATE edison_pull_log SET file_type = "Receipts" WHERE time_elapsed IS NULL;''')
    s3.download_file('superdata-receipts-data', i, f'{path}/{x}_00{files.index(i)}.csv.gz')

    #UNZIP
    with gzip.open(f'{path}/{x}_00{files.index(i)}.csv.gz', 'rb') as f_in:
        with open(f'{path}/{x}_00{files.index(i)}.csv', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
            os.remove(f'{path}/{x}_00{files.index(i)}.csv.gz')

    ##WRITE TO TABLE
    cursor.execute(f'''LOAD DATA LOCAL INFILE '{path}/{x}_00{files.index(i)}.csv' INTO TABLE import_edison_data.receipts_raw FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES;''')
    cursor.execute('''SET @max_time_start = (SELECT MAX(time_start) FROM edison_pull_log);''')
    cursor.execute('''UPDATE edison_pull_log SET time_elapsed = TIMEDIFF(CONVERT_TZ(NOW(),'UTC', 'America/New_York'), @max_time_start) WHERE time_elapsed IS NULL;''')

    #UPLOAD TO S3 AND REMOVE FROM INSTANCE
    s3_writer.upload_file(f'{path}/{x}_00{files.index(i)}.csv', bucket_name, f'{directory_name}/{x}_00{files.index(i)}.csv')
    os.remove(f'{path}/{x}_00{files.index(i)}.csv')

cursor.execute("UPDATE import_edison_data.receipts_raw A SET A.mnth = MONTH(email_time), A.yr = YEAR(email_time), A.qtr = QUARTER(email_time);")

cursor.execute("CALL import_edison_data.process_users_gmail();")
cursor.execute("CALL import_edison_data.process_receipts_gmail();")

cursor.execute("CALL import_edison_data.edison_summary_master();")
